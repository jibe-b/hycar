// ==UserScript==
// @name         Extract url of carnets de recherche
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.openedition.org/*?script=extract_urls_carnets_recherche
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const carnets = JSON.parse(localStorage.getItem("carnets"))

    //localStorage.setItem("page_index", JSON.stringify(0))
    //localStorage.setItem("carnets_urls", JSON.stringify([]))
    //window.open(carnets[0] + "?script=extract_urls_carnets_recherche", "_self")

    const extract = () => {

        const carnets_urls = JSON.parse(localStorage.getItem("carnets_urls"))

        const page_index = JSON.parse(localStorage.getItem("page_index"))

        const url_carnet = document.querySelectorAll(".read-more")[0].href


        carnets_urls.push(carnets[page_index] + "," + url_carnet)

        localStorage.setItem("carnets_urls", JSON.stringify(carnets_urls))

        const next_index = page_index + 1
        localStorage.setItem("page_index", JSON.stringify(next_index))
        window.open(carnets[next_index] + "?script=extract_urls_carnets_recherche", "_self")

    }

    window.setInterval(extract, 2000)
})();
